#include <iostream>
#include <ctime>

void PrintIntroduction(int LevelDifficulty) 
{
    std::cout << "\n\n Agent X:  You are to break in to a level " << LevelDifficulty << " secure server room. " << std::endl;
    std::cout << "You will need to enter the correct codes to continue... "  << std::endl;
}

bool PlayGame(int LevelDifficulty) 
{
    PrintIntroduction(LevelDifficulty);
    
    const int CodeA = rand() % LevelDifficulty + LevelDifficulty;
    const int CodeB = rand() % LevelDifficulty + LevelDifficulty;
    const int CodeC = rand() % LevelDifficulty + LevelDifficulty;

    const int CodeSum = CodeA + CodeB + CodeC;
    const int CodeProduct = CodeA * CodeB * CodeC;

    std::cout << std::endl;
    std::cout << "There are 3 numbers in the code" << std::endl;
    std::cout << "=> The codes add up to: " << CodeSum << std::endl;
    std::cout << "and multiply to give: " << CodeProduct << std::endl;

    int GuessA, GuessB, GuessC;
    std::cin >> GuessA;
    std::cin >> GuessB;
    std::cin >> GuessC;

    int GuessSum = GuessA + GuessB + GuessC;
    int GuessProduct = GuessA * GuessB * GuessC;

    if(GuessSum == CodeSum && GuessProduct == CodeProduct)
    {
        std::cout << "That's correct - advancing to the next level";
        return true;
    } 
    else 
    {
        std::cout << "No - that's wrong - try again!";
        return false;
    }
}

int main() 
{
    srand(time(NULL));
    int LevelDifficulty = 1;
    const int MaxLevel = 20;

    while (LevelDifficulty <= MaxLevel)
    {
        bool bLevelComplete = PlayGame(LevelDifficulty);
        std::cin.clear();
        std::cin.ignore();

        if (bLevelComplete) 
        {
            ++LevelDifficulty;
        }
    }

    std::cout << "Well done.  You've cracked into the top secret bunker.";

    return 0;
}
